REGION_CPU = 0
REGION_PPU = 1
class Driver
	def cpu
		@cpu
	end
	def ppu
		@ppu
	end
end

module Dummy
	def debugout(rw, address, data, address_increment)
		if address_increment != 1
			while data.size > 0
				debugout(rw, address, data.shift, 1)
				address += address_increment
			end
			return
		end
		#整数同士を除算すると float になってしまうらしい...
		if data.instance_of?(Fixnum) || data.instance_of?(Float)
			data = [data]
		end
		str = sprintf("%s.%s A:%s%04x", @region, rw, @address_prefix, address)
		if data.size != 1
			if address_increment == 0
				str << sprintf("x%d", data.size)
			else
				str << sprintf("-%s%04x", @address_prefix, address + data.size - 1)
			end
		end
		if rw[rw.size - 1] == "W"
			str << " D:"
			data.each{|t|
				str << sprintf("$%02x ", t)
			}
			str.chop!
		end
		puts str
	end
end
module Dummy_register_rw
	def register_write(address, data, address_increment = 1, address_mask = 0xffff)
		debugout('REG.W', address, data, address_increment)
	end
=begin
	def register_read(address, size, address_increment = 1, address_mask = 0xffff)
		data = Array.new(size, 0x0e)
		debugout('REG.R', address, data, address_increment)
	end
=end
end
module Bus
	def memory_read(address, size)
		busreader_memory_read(@bus_region, address, size, @rom)
	end
end
module Bus_register_write
	def register_write(address, data, increment = 1, mask = 0xffff)
		if data.instance_of?(Fixnum) || data.instance_of?(Float)
			data = [data]
		end
		data.map!{|t|
			t.to_i
		}
		busreader_cpu_register_write address, data, increment, mask
	end
end
module Bus_test_cpu
	def dump_test
		src0 = []
		busreader_memory_read(@bus_region, 0x8000, 4, src0)
		3.times{
			src1 = []
			busreader_memory_read(@bus_region, 0x8000, 4, src1)
			if src0 != src1
				return false
			end
		}
		true
	end
end
module Bus_memory_is_ram
	def memory_is_ram?
		a = 0x0123
		wdata = []
		busreader_memory_read(@bus_region, a, 4, wdata)
		wdata.map!{|t|
			t ^ 0xff
		}
		busreader_memory_write(@bus_region, a, wdata)
		rdata = []
		busreader_memory_read(@bus_region, a, wdata.size, rdata)
		wdata == rdata
	end
end
module Bus_open
	def bus_open_
		if busreader_open == false
			puts "cannot open busreader"
			return false
		end
		if @cpu.dump_test == false
			puts "maybe loose connection"
			return false
		end
		true
	end
end
