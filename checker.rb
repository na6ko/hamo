class Checker
	def Rom
		@rom
	end
	def address_range_check(min, max, target)
		if target < min || target >= max
			raise StandardError.new sprintf("%s address range error %s%x\n", @nane, @address_prefix, target)
		end
	end
	def rom_append(size)
		@rom.concat Array.new(size)
	end
end
=begin
	def memory_push(data)
		if data.instance_of?(Fixnum)
			@rom << data
		elsif data.instance_of?(Array)
			@rom.concat data
		else
			raise StandardError.new("data is not integer or Array")
		end
	end
	def register_read(address, size, address_increment = 1, address_mask = 0xffff)
		register_rw(address, Array.new(size, 0), address_increment, address_mask)
	end
=end
class Checker_cpu < Checker
	def initialize
		@rom = []
		@name = "CPU"
		@address_prefix = "$"
	end
	def register_rw(address, data, address_increment, address_mask)
		if data.instance_of?(Fixnum)
			data = [data]
		elsif data.instance_of?(Array)
			#do nothing
		else
			raise StandardError.new("data is not integer or Array")
		end
		data.each{|t|
			address_range_check 0x4000, 0x10000, address
			if t < 0 || t >= 0x100
				StandardError.new "CPU data range error " + sprintf("address: 0x%04x, data : 0x%02x\n", address, data)
			end
			address += address_increment
			address &= address_mask
		}
	end
	def register_write(address, data, address_increment = 1, address_mask = 0xffff)
		register_rw(address, data, address_increment, address_mask)
	end
end
class Checker_cpu_dumper < Checker_cpu
	def initialize
		super
	end
	def memory_read(address, size)
		address_range_check 0x6000, 0x10000, address
		address_range_check 0x6000, 0x10000, address + size - 1
		rom_append(size)
	end
end
module Checker_programmer
	def flash_command_check(flash_a)
		case flash_a
		when 0, 0x02aa, 0x2aaa, 0x0555, 0x5555
			#do nothing
		else
			raise StandardError.new sprintf("%s unknown flash command address: %04x\n", flash_a)
		end
	end
	def flash_programming_start
	end
	def flash_size
		@flash_size
	end
end
class Checker_cpu_programmer < Checker_cpu
	include Checker_programmer
	def initialize
		super
		@flash_size = 0
	end
	def flash_command_set(flash_a, cpu_a, banksize)
		flash_command_check flash_a
		address_range_check 0x8000, 0x10000, cpu_a
	end
	def flash_program(cpu_a, size)
		address_range_check 0x8000, 0x10000, cpu_a
		address_range_check 0x8000, 0x10000, cpu_a + size - 1
		@flash_size += size
	end
end

class Checker_ppu < Checker
	def initialize
		@rom = []
		@name = "PPU"
		@address_prefix = "0x"
	end
	def memory_is_ram?
		false
	end
end
class Checker_ppu_dumper < Checker_ppu
	def initialize
		super
	end
	def memory_read(address, size)
		address_range_check 0x0000, 0x3000, address
		address_range_check 0x0000, 0x3000, address + size - 1
		rom_append(size)
	end
end
class Checker_ppu_programmer < Checker_ppu
	include Checker_programmer
	def initialize
		super
		@flash_size = 0
	end
	def flash_command_set(flash_a, ppu_a, banksize)
		flash_command_check flash_a
		address_range_check 0x0000, 0x2000, ppu_a
	end
	def flash_program(ppu_a, size)
		address_range_check 0x0000, 0x2000, ppu_a
		address_range_check 0x0000, 0x2000, ppu_a + size - 1
		@flash_size += size
	end
end

class Driver_checker < Driver
	def syntax(cpu_size, ppu_size)
		cpu_syntax = Board[:cpu_rom][:managed_rom_size] == cpu_size
		ppu_syntax = Board[:ppu][:managed_rom_size] == ppu_size
		if cpu_syntax && ppu_syntax
			return true
		end
		puts("loaded memory size is not match")
		printf("CPU 0x%05x/0x%05x\n", cpu_size, Board[:cpu_rom][:managed_rom_size])
		printf("PPU 0x%05x/0x%05x\n", ppu_size, Board[:ppu][:managed_rom_size])
		false
	end
end
class Driver_checker_dumper < Driver_checker
	def initialize
		@cpu = Checker_cpu_dumper.new
		@ppu = Checker_ppu_dumper.new
	end
	def driver_rom_dump
		if Board[:rom_dump_auto] == true
			return true
		end
		rom_dump self, false
		syntax @cpu.Rom.size, @ppu.Rom.size
	end
end
class Driver_checker_programmer < Driver_checker
	def initialize
		@cpu = Checker_cpu_programmer.new
		@ppu = Checker_ppu_programmer.new
	end
	def driver_flash_program(cpu_layout, ppu_layout)
		rom_register_init self
		cpu_size = Board[:cpu_rom][:managed_rom_size]
		if Board[:cpu_rom][:flash_program_auto] == false
			flash_program_cpu self, 0, Board[:cpu_rom][:managed_rom_size]
			cpu_size = @cpu.flash_size
		end
		
		ppu_size = Board[:ppu][:managed_rom_size]
		if Board[:ppu][:flash_program_auto] == false
			flash_program_ppu self, 0, Board[:ppu][:managed_rom_size]
			ppu_size = @ppu.flash_size
		end
		syntax cpu_size, ppu_size
	end
end
