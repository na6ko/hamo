ERASE_OP_NONE = 0
ERASE_OP_CHIP = 1
ERASE_OP_SECTOR = 2

PROGRAM_LAYOUT_SKIP = 0
PROGRAM_LAYOUT_FULL = 1
PROGRAM_LAYOUT_TOP = 2
PROGRAM_LAYOUT_BOTTOM = 3

class FlashMemory
	FLASH_DEVICE = {
		"512K" => {
			:id => nil, :command_width => 14, :pagesize => 1,
			:device_command => false, :sectorsize => nil,
			:capacity => 0x80000, :erase_required => true
		}, "AM29F040B" => { #AMD, Fujitsu
			:id => [0x01, 0xa4], :command_width => 10, :pagesize => 1,
			:device_command => true, :sectorsize => 0x10000,
			:capacity => 0x80000, :erase_required => true
			#sector-erase: 1000 ms, chip-erase 8000 ms, byte-programming: 7 us
		}, "W29C040" => { #Winbond
			:id => [0xda, 0x46], :command_width => 14, :pagesize => 0x100,
			:device_command => false, :sectorsize => nil,
			:capacity => 0x80000, :erase_required => false
			#page-program: 5 ms, chip-erase: 50 ms(?)
		}, "EN29F002T" => { #EON
			:id => [0x1c, 0x92], :command_width => 10, :pagesize => 1,
			:device_command => true, :sectorsize => [0x10000, 0x10000, 0x10000, 0x8000, 0x2000, 0x2000, 0x4000],
			:capacity => 0x40000, :erase_required => true
			#sector-erase: 500 ms, chip-erase: 3500 ms, byte-program: 10us
		}, "SST39SF040" => { #SST, Microchip
			:id => [0xbf, 0xb5], :command_width => 14, :pagesize => 1,
			:device_command => true, :sectorsize => 0x01000,
			:capacity => 0x80000, :erase_required => true
			#sector-erase: 18 ms, chip-erase: 70 ms, byte-program: 14us
		}, "MX29F040C" => { #Macronix
			:id => [0x90, 0xa4], :command_width => 10, :pagesize => 1,
			:device_command => true, :sectorsize => 0x10000,
			:capacity => 0x80000, :erase_required => true
			#sector-erase: 700 ms, chip-erase: 4000 ms, byte-program: 9us
		}, "A29040B" => { #AMIC
			:id => [0x37, 0x86], :command_width => 10, :pagesize => 1,
			:device_command => true, :sectorsize => 0x10000,
			:capacity => 0x80000, :erase_required => true
			#sector-erase: 1000 ms, chip-erase 8000 ms, byte-programming: 7 us
		}
	}
	def initialize(d)
		@driver = d
		@device_detect = true
		en29 = FLASH_DEVICE["EN29F002T"].dup
		en29[:id] = [en29[:id][0], 0x97]
		en29[:sectorsize] = en29[:sectorsize].reverse
		FLASH_DEVICE["EN29F002B"] = en29
		
		sst = FLASH_DEVICE["SST39SF040"].dup
		["SST39SF020A", "SST39SF010A"].each{|name|
			sst[:capacity] /= 2
			sst[:id] = sst[:id].dup
			sst[:id][1] -= 1
			FLASH_DEVICE[name] = sst.dup
		}
		@flash_device_list_byid = {}
		FLASH_DEVICE.each{|key, tt|
			t = tt.dup
			t[:name] = key
			@flash_device_list_byid[t[:id]] = t
		}
		@erase_operation = ERASE_OP_CHIP
	end
	def programming_unit_get
		@device[:pagesize]
	end
	def device_set(arg)
		@erase_operation = arg[:erase]
		if arg[:name] == "auto"
			return true
		end
		@device_detect = false
		@device = FLASH_DEVICE[arg[:name]]
		if FLASH_DEVICE.key?(arg[:name]) == false
			printf "device %s is not found\n", name
			return false
		end
		if @erase_operation == ERASE_OP_SECTOR && @device[:sectorsize] == nil
			printf "%s is not supported sector-erase\n", name
			@erase_operation = ERASE_OP_CHIP
		end
		return true
	end
	def programming_start(offset)
		@offset = offset
		if @device_detect == true
			id = @driver.flash_id_get
			@device = @flash_device_list_byid[id]
			if @device == nil
				printf "unknown device id: 0x%02x 0x%02x\n", id[0], id[1]
				return false
			end
		end
		if @device_detect == true
			printf "%s.flash device:%s\n", @driver.Region, @device[:name]
		end
		@device[:sector_erase_address] = []
		if @erase_operation == ERASE_OP_CHIP || @device[:sectorsize] == nil
			@driver.flash_erase_chip
		elsif @device[:sectorsize].instance_of?(Fixnum)
			offset = 0
			while offset < @device[:capacity]
				@device[:sector_erase_address] << (offset...offset + @device[:sectorsize])
				offset += @device[:sectorsize]
			end
			#p @device[:sector_erase_address]
		elsif @device[:sectorsize].instance_of?(Array)
			offset = 0
			@device[:sectorsize].each{|t|
				@device[:sector_erase_address] << (offset...offset+t)
				offset += t
			}
		else
			puts @device[:name] + "[:sectorsize] is not nil, fixnum or array"
			return false
		end

		return true
	end
	def program_data(bus_a, size, str, lastbank)
		if lastbank != nil
			@offset = [@device[:capacity], lastbank].min
			@offset -= size
		end
		if @erase_operation == ERASE_OP_SECTOR
			of = @offset
			s = size
			b = bus_a
			while s > 0
				@device[:sector_erase_address].delete_if{|t|
					r = t.include? of
					if r == true
						@driver.flash_erase_sector b, of
					end
					r
				}
				b += 0x1000
				of += 0x1000
				s -= 0x1000
			end
		end
		@driver.flash_program_from_memory bus_a, @offset, size, str
		@offset += size
	end
	def id_to_name(id)
		if @flash_device_list_byid.key?(id) == false
			return sprintf "unknown (0x%02x 0x%02x)", id[0], id[1]
		end
		"flash memory " + @flash_device_list_byid[id][:name]
	end
end

class Programmer
	def initialize rommap
		@device = FlashMemory.new self
		@rom = []
		@command = {}
		@rommap = rommap
	end
	def Region
		@region
	end
	def rom_set(rom)
		@src_offset = 0
		@src_mask = rom.size - 1
		@rom = rom
		printf "%s romsize 0x%x\n", @region, @rom.size
	end
	def layout_get(layout, managed_size)
		dest_offset = nil
		dest_size = nil
		case layout
		when PROGRAM_LAYOUT_SKIP
			#do nothing
		when PROGRAM_LAYOUT_FULL
			dest_offset = 0
			dest_size = managed_size
		when PROGRAM_LAYOUT_TOP
			dest_size = @rom.size
			dest_offset = 0
		when PROGRAM_LAYOUT_BOTTOM
			dest_size = @rom.size
			dest_offset = managed_size
			dest_offset -= dest_size
			#printf "%x %x\n", dest_size, dest_offset
		end
		@dest_offset = dest_offset
		@dest_size = dest_size
		return dest_offset, dest_size
	end
=begin
device_name.erase_option
erase_option is chip,sector or none
chip: do chip-erase, default
sector: do sector_erase
none: do not erase
=end
	ERASE_NAME_TO_OP = {
		'c' => ERASE_OP_CHIP,
		"chip" => ERASE_OP_CHIP,
		's' => ERASE_OP_SECTOR,
		"sector" => ERASE_OP_SECTOR,
		'n' => ERASE_OP_NONE,
		"none" => ERASE_OP_NONE
	}
	LAYOUT_NAME_TO_OP = {
		'f' => PROGRAM_LAYOUT_FULL,
		'full' => PROGRAM_LAYOUT_FULL,
		'top' => PROGRAM_LAYOUT_TOP,
		't' => PROGRAM_LAYOUT_TOP,
		'bottom' => PROGRAM_LAYOUT_BOTTOM,
		'b' => PROGRAM_LAYOUT_BOTTOM,
		'e' => PROGRAM_LAYOUT_SKIP,
		'skip' => PROGRAM_LAYOUT_SKIP
	}
	def part layout
		r = {:layout => PROGRAM_LAYOUT_FULL, :name => "512K", :erase => ERASE_OP_CHIP}
		if layout == nil
			return r
		end
		t = layout.split(':')
		r[:layout] = LAYOUT_NAME_TO_OP[t[0]]
		if r[:layout] == nil
			printf "unknown layout: %s\n", t[0]
			return nil
		end
		if t.size < 2
			return r
		end
		r[:name] = t[1]
		if t.size < 3
			return r
		end
		r[:erase] = ERASE_NAME_TO_OP[t[2]]
		if r[:erase] == nil
			printf "unknown erase operation: %s\n", t[2]
			return nil
		end
		return r
	end
	def flash_programming_device_init(layout)
		@config = part layout
		if @config == nil
			return nil
		end
		if @config[:layout] == PROGRAM_LAYOUT_SKIP
			return true
		end
		if @device.device_set(@config) == false
			return nil
		end
		return false
	end
	def flash_programming_loop_init(d)
		@fiber = nil
		status = @config[:layout] == PROGRAM_LAYOUT_SKIP
		if status == false
			if @rommap[:flash_program_auto] == false
				@fiber = Fiber.new{
					descripted_flash_program d, @config[:layout]
					Fiber.yield true
				}
			else
				@fiber = Fiber.new{
					auto_flash_program d, @config[:layout]
					Fiber.yield true
				}
			end
		end
		status
	end
	def flash_programming_loop_resume
		if @fiber == nil
			return true
		end
		if @fiber.alive? == false
			return true
		end
		@fiber.resume
	end
	def memory_detection_rom
		rom = []
		busreader_memory_read(@bus_region, @rommap[:rom_bank][0][:address], 2, rom)
		rom
	end
	def memory_detection d
		if memory_detection_ram
			printf "%s: %s\n", @region, "RAM"
			return
		end
		if @rommap.size < 3
			printf "note: %s side has 2 or less switchable bank(s), flash memory command address range must be A10:0 for device ID detection\n", @region
		end
		command_bank_set d
		id = flash_id_get
		rom = memory_detection_rom
		str = @region + ': '
		if rom == id
			str << sprintf("ROM (0x%02 %0x02x) or ", id[0], id[1])
		end
		str << @device.id_to_name(id)
		puts str
	end
end
module Dummy_programmer
	def flash_command_set(flash_a, bus_a, banksize)
		printf "%s.COMMAND.%04x <- %s%04x\n", @region, flash_a, @address_prefix, bus_a | ((banksize - 1) & flash_a)
	end
	def flash_programming_start
		printf "%s.PROGRAMMING.START\n", @region
		if @device.programming_start(@dest_offset) == false
			Fiber.yield true
		end
		Fiber.yield false
	end
	def flash_program_memory(bus_a, size)
		str = sprintf "%s.PROGRAM a:%s%04x-%s%04x s:0x%05x-0x%05x", @region, @address_prefix, bus_a, @address_prefix, bus_a + size - 1, @src_offset, @src_offset + size - 1
		@device.program_data(bus_a, size, str, lastbank?(bus_a))
		@src_offset += size
		@src_offset &= @src_mask
	end
	def flash_program_from_memory(bus_a, dest_offset, size, str)
		printf "%s d:0x%05x-0x%05x\n", str, dest_offset, dest_offset + size - 1
		Fiber.yield false
	end
	def flash_erase_sector(bus_a, dest_offset)
		printf("%s.SECTOR_ERASE d:0x%05x\n", @region, dest_offset)
		Fiber.yield false
	end
	def flash_erase_chip
		printf("%s.CHIP_ERASE\n", @region)
		Fiber.yield false
	end
	def flash_id_get
		[0x01, 0xa4]
		#[0xbf, 0xb3]
	end
end

module Bus_programmer
	READER_PROGRAMMING_UNIT = 0x100
	PROGRAM_FF = Array.new(READER_PROGRAMMING_UNIT, 0xff)
	ERASE_AREA_CHIP = 0
	ERASE_AREA_SECTOR = 1
	def flash_command_set(flash_a, bus_a, banksize)
		@command[flash_a] = bus_a | ((banksize - 1) & flash_a)
		@command_update = true
	end
	def flash_programming_start
		gauge_range_set @bus_region, @dest_size
		if @device.programming_start(@dest_offset) == false
			Fiber.yield true
		end
		Fiber.yield false
	end
	def flash_program_memory(bus_a, size)
		@device.program_data(bus_a, size, nil, lastbank?(bus_a))
		@src_offset += size
		@src_offset &= @src_mask
	end
	def command_reset
		if @command_update == false
			return
		end
		@command_update = false
		c0 = @command[0]
		c2 = @command[0x02aa]
		c5 = @command[0x0555]
		if @command.key?(0x2aaa)
			c2 = @command[0x2aaa]
		end
		if @command.key?(0x5555)
			c5 = @command[0x5555]
		end
		busreader_flash_command_set(@bus_region, c0, c2, c5, @device.programming_unit_get)
		c5
	end
	def program_sync
		Fiber.yield false
		while busreader_flash_status(@bus_region) == false
			Fiber.yield false
		end
	end
	def flash_program_from_memory(bus_a, dest_offset, size, str)
		command_reset
		offset = @src_offset
		while size > 0
			src = @rom[offset, READER_PROGRAMMING_UNIT]
			skip = src == PROGRAM_FF
			if skip
				busreader_flash_program_skip(@bus_region, src.size)
			else
				busreader_flash_program_data(@bus_region, bus_a, src)
			end
			bus_a += READER_PROGRAMMING_UNIT
			offset += READER_PROGRAMMING_UNIT
			size -= READER_PROGRAMMING_UNIT
			if skip == false
				program_sync
			end
		end
	end
	def flash_erase_sector(bus_a, dest_offset)
		command_reset
		busreader_flash_erase(@bus_region, ERASE_AREA_SECTOR, bus_a)
		program_sync
	end
	def flash_erase_chip
		c5 = command_reset
		printf "%x\n\n",c5
		busreader_flash_erase(@bus_region, ERASE_AREA_CHIP, c5)
		program_sync
	end
	def flash_id_get
		busreader_flash_id_get(@bus_region)
	end
end

class Programmer_cpu < Programmer
	def initialize rommap
		@region = "CPU"
		@address_prefix = '$'
		@bus_region = REGION_CPU
		super rommap
	end
	def lastbank?(bus_a)
		@rommap[:rom_bank].last[:address] == bus_a ? 
			@rommap[:managed_rom_size] : nil
	end
	def descripted_flash_program d, layout
		if layout == PROGRAM_LAYOUT_SKIP
			Fiber.yield true
		end
		dest_offset, dest_size = layout_get cpu_device[:layout], @rommap[:managed_rom_size]
		if dest_offset != nil
			flash_program_cpu d, dest_offset, dest_size
		end
		Fiber.yield true
	end
	def command_bank_set(d)
		t = @rommap[:rom_bank]
		if t.size < 3
			[0, 0x02aa, 0x0555].each{|s|
				flash_command_set(s, t.last[:address], t.last[:size])
			}
		else
			aa = [0x0000, 0x2aaa, 0x5555]
			[t[0], t[1], t.last].each{|s|
				a = aa.shift
				flash_command_set(a, s[:address], s[:size])
				rom_bank_control_cpu(d, a, s[:address], s[:size])
			}
		end
	end
	def memory_detection_ram
		false
	end
	def auto_flash_program d, layout
		if layout == PROGRAM_LAYOUT_SKIP
			Fiber.yield true
		end
		t = @rommap[:rom_bank]
		command_bank_set d
		
		bank = t[0]
		offset, length = layout_get(layout, @rommap[:managed_rom_size])
		if offset == nil
			Fiber.yield true
		end

		flash_programming_start
		while length > t.last[:size]
			rom_bank_control_cpu(d, offset, bank[:address], bank[:size])
			flash_program_memory(bank[:address], bank[:size])
			offset += bank[:size]
			length -= bank[:size]
		end
		
		bank = t.last
		rom_bank_control_cpu(d, offset, bank[:address], bank[:size])
		flash_program_memory(bank[:address], bank[:size])
	end
end
class Bus_programmer_cpu < Programmer_cpu
	include Bus_programmer
	include Bus_register_write
	include Bus_test_cpu
end
class Dummy_programmer_cpu < Programmer_cpu
	include Dummy
	include Dummy_programmer
	include Dummy_register_rw
end
class Programmer_ppu < Programmer
	def initialize rommap
		@region = "PPU"
		@address_prefix = '0x'
		@bus_region = REGION_PPU
		super rommap
	end
	def lastbank?(a)
		nil
	end
	def memory_detection_ram
		memory_is_ram?
	end
	def command_bank_set(d)
		t = @rommap[:rom_bank]
		case t.size 
		when 0
			return
		when 1
			[0, 0x02aa, 0x0555].each{|s|
				flash_command_set(s, t[0][:address], t[0][:size])
			}
		when 2
			flash_command_set(0, t[0][:address], t[0][:size])
			i = 0
			[0x02aa, 0x0555].each{|c|
				flash_command_set(c, t[i][:address], t[i][:size])
				rom_bank_control_ppu(d, c, s[:address], s[:size])
				i += 1
			}
		else
			aa = [0x0000, 0x2aaa, 0x5555]
			t.each{|s|
				a = aa.shift
				flash_command_set(a, s[:address], s[:size])
				rom_bank_control_ppu(d, a, s[:address], s[:size])
			}
		end
	end
	def auto_flash_program d, layout
		if layout == PROGRAM_LAYOUT_SKIP
			Fiber.yield true
		end
		t = @rommap[:rom_bank]
		program_bank = nil
		case t.size
		when 0
			Fiber.yield true
		when 1
			program_bank = t[0]
			ss = t[0]
			a = ss[:address]
			s = ss[:size]
			[0, 0x02aa, 0x0555].each{|s|
				flash_command_set(s, t[0][:address], t[0][:size])
			}
		when 2
			#command address 14:0 では ID 取得不可
			flash_command_set(0x2aaa, t[0][:address], t[0][:size])
			flash_command_set(0x5555, t[1][:address], t[1][:size])
		else
			program_bank = t[2]
			aa = [0x0000, 0x2aaa, 0x5555]
			[t[2], t[0], t[1]].each{|s|
				a = aa.shift
				flash_command_set(a, s[:address], s[:size])
				rom_bank_control_ppu(d, a, s[:address], s[:size])
			}
		end
		
		offset, length = layout_get(layout, @rommap[:managed_rom_size])
		if offset == nil
			Fiber.yield true
		end
		flash_programming_start
		if program_bank != nil
			while length > 0
				rom_bank_control_ppu(d, offset, program_bank[:address], program_bank[:size])
				flash_program_memory(program_bank[:address], program_bank[:size])
				offset += program_bank[:size]
				length -= program_bank[:size]
			end
		else
			while length > 0
				bank0 = offset
				bank1 = 0x5555
				program_bank = t[0]
				if (offset & 0x1000) != 0
					bank0 = 0x2aaa
					bank1 = offset
					program_bank = t[1]
				end
				rom_bank_control_ppu(d, bank0, t[0][:address], t[0][:size])
				rom_bank_control_ppu(d, bank1, t[1][:address], t[1][:size])
				flash_program_memory(program_bank[:address], program_bank[:size])
				offset += program_bank[:size]
				length -= program_bank[:size]
			end
		end
	end
end
class Dummy_programmer_ppu < Programmer_ppu
	include Dummy
	include Dummy_programmer
end

class Bus_programmer_ppu < Programmer_ppu
	include Bus_programmer
	include Bus_memory_is_ram
end

module Driver_programmer
	def driver_flash_program(cpu_layout, ppu_layout)
		cpu_status = @cpu.flash_programming_device_init cpu_layout
		ppu_status = @ppu.flash_programming_device_init ppu_layout
		
		if cpu_status == nil || ppu_status == nil
			return
		end
		if bus_open_ == false
			return
		end
		rom_register_init self
		cpu_status = @cpu.flash_programming_loop_init self
		ppu_status = @ppu.flash_programming_loop_init self
		
		while cpu_status == false || ppu_status == false
			cpu_status = @cpu.flash_programming_loop_resume
			ppu_status = @ppu.flash_programming_loop_resume
		end
		
		bus_close
	end
	def driver_memory_detection
		if bus_open_ == false
			return
		end
		rom_register_init self
		@cpu.memory_detection self
		@ppu.memory_detection self
		bus_close
	end
end
class Driver_dummy_programmer < Driver
	include Driver_programmer
	def initialize
		@cpu = Dummy_programmer_cpu.new Board[:cpu_rom]
		@ppu = Dummy_programmer_ppu.new Board[:ppu]
	end
	def bus_open_
		true
	end
	def bus_close
	end
end
class Driver_bus_programmer < Driver
	include Driver_programmer
	include Bus_open
	def initialize
		@cpu = Bus_programmer_cpu.new Board[:cpu_rom]
		@ppu = Bus_programmer_ppu.new Board[:ppu]
	end
	def bus_close
		busreader_close
	end
end
