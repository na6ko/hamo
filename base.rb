#_require "./driver.rb"
def base_rom_dump(e, destfilename)
	d = Driver_checker_dumper.new
	if d.driver_rom_dump == false
		return
	end
	if e.driver_rom_dump == false
		return
	end
	e.nesfile_save(destfilename) 
end
def reader_rom_dump(destfilename)
	d = Driver_bus_dumper.new
	base_rom_dump(d, destfilename)
end
def dummy_rom_dump(destfilename)
	d = Driver_dummy_dumper.new
	base_rom_dump(d, destfilename)
end
def base_flash_program(d, srcfilename, cpu_layout, ppu_layout)
	e = Driver_checker_programmer.new
	if e.driver_flash_program(PROGRAM_LAYOUT_FULL, PROGRAM_LAYOUT_FULL) == false
		return
	end
	
	if File.exist?(srcfilename) == false
		printf "image file %s is not found\n", srcfilename
		return
	end
	f = File.open(srcfilename, 'rb')
	header = f.read(0x10).unpack('C*')
	if header[0,4] != [0x4e, 0x45, 0x53, 0x1a]
		puts "NES header error"
		return
	end
	nm = {}
	7.times{|i|
		nm[1 << i] = i
	}
	if nm.key?header[4] == false
		puts "CPU memory size is not 2**n"
		return
	end
	nm[0] = 0
	if nm.key?header[5] == false
		puts "PPU memory size is not 2**n"
		return
	end
	d.cpu.rom_set f.read(header[4] * 0x4000).unpack('C*')
	if header[5] == 0
		ppu_layout = 'skip'
	else
		d.ppu.rom_set f.read(header[5] * 0x2000).unpack('C*')
	end
	f.close
	d.driver_flash_program cpu_layout, ppu_layout
end
def dummy_flash_program(srcfilename, cpu_layout = nil, ppu_layout = nil)
	base_flash_program(Driver_dummy_programmer.new, srcfilename, cpu_layout, ppu_layout)
end
def reader_flash_program(srcfilename, cpu_layout = nil, ppu_layout = nil)
	base_flash_program(Driver_bus_programmer.new, srcfilename, cpu_layout, ppu_layout)
end
def reader_memory_detection
	Driver_bus_programmer.new.driver_memory_detection
end
