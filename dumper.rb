class Dumper
	def memory_unmirror(min, rom)
		while rom.size > min
			s = rom.size / 2
			if rom[0, s] != rom[s, s]
				break
			end
			rom = rom[0, s]
		end
		rom
	end
	def Rom
		memory_unmirror(@unmirror_min, @rom)
	end
end
class Dumper_cpu < Dumper
	def initialize
		@rom = []
		@region = "CPU"
		@address_prefix = '$'
		@unmirror_min = 0x4000
	end
	def auto_rom_dump d
		t = Board[:cpu_rom][:rom_bank]
		bank = t[0]
		offset = 0
		while offset < Board[:cpu_rom][:managed_rom_size] - t.last[:size]
			rom_bank_control_cpu(d, offset, bank[:address], bank[:size])
			memory_read(bank[:address], bank[:size])
			offset += bank[:size]
		end
		
		bank = t.last
		rom_bank_control_cpu(d, offset, bank[:address], bank[:size])
		memory_read(bank[:address], bank[:size])
	end
end
class Dummy_dumper_cpu < Dumper_cpu
	include Dummy
	include Dummy_register_rw
	def initialize
		super
	end
	def memory_read(address, size)
		data = Array.new(size, 0x0f)
		debugout('MEM.R', address, data, 1)
		@rom.concat data
	end
end
class Dumper_ppu < Dumper
	def initialize
		@rom = []
		@region = "PPU"
		@address_prefix = '0x'
		@unmirror_min = 0x2000
	end
	def auto_rom_dump d
		offset = 0
		while offset < Board[:ppu][:managed_rom_size]
			rom_bank_control_ppu(d, offset, 0, 0x2000)
			memory_read(0, 0x2000)
			offset += 0x2000
		end
	end
end
class Dummy_dumper_ppu < Dumper_ppu
	include Dummy
	def initialize
		super
	end
	def memory_is_ram?
		false
	end
	def memory_read(address, size)
		data = Array.new(size, 0xf0)
		debugout('MEM.R', address, data, 1)
		@rom.concat data
	end
end
class Bus_dumper_cpu < Dumper_cpu
	include Bus
	include Bus_register_write
	include Bus_test_cpu
	def initialize
		super
		@bus_region = REGION_CPU
	end
end
class Bus_dumper_ppu < Dumper_ppu
	include Bus
	include Bus_memory_is_ram
	def initialize
		super
		@bus_region = REGION_PPU
	end
end

module Driver_dumper
	VRAM_CONNECTION = {nil => "software control", 0 => "Horizontal", 1 => "Vertical"}
	def nesfile_save(destfilename)
		cpu_rom = @cpu.Rom
		ppu_rom = @ppu.Rom

		allff = Array.new(0xff, [cpu_rom.size, ppu_rom.size].max)
		ret = true
		if cpu_rom == allff[0, cpu_rom.size]
			puts "CPU area ROM data is all 0xff"
			ret = false
		end
		if ppu_rom.size != 0 && ppu_rom == allff[0, ppu_rom.size]
			puts "PPU area ROM data is all 0xff"
			ret = false
		end
		if ret == false
			return
		end

		header = [0x4e, 0x45, 0x53, 0x1a,
			cpu_rom.size / 0x4000,
			ppu_rom.size / 0x2000,
			(Board[:ines_mapper_number] << 4) & 0b1111_0000, #6
			Board[:ines_mapper_number] & 0b1111_0000, #7
			0, 0, 0, 0,
			0, 0, 0, 0
		]
		puts "               data size     CRC32"
		printf("Program ROM:   0x%05X byte, 0x%08X\n", cpu_rom.size, crc32_calc(cpu_rom))
		if ppu_rom.size == 0
			puts "Character RAM"
		else
			printf("Character ROM: 0x%05X byte, 0x%08X\n", ppu_rom.size, crc32_calc(ppu_rom))
		end
		if Board[:vram_a10_control] == false
			printf "VRAM connection: %s\n", VRAM_CONNECTION[@vram_a10]
			header[6] |= @vram_a10
		end
		#todo: support NES 2.0 header
		if Board[:ines_mapper_number] >= 0x100 || Board.key?(:nes2_0_submapper_number) == true
			header[7] |= 0b10 << 2 #identifier
			header[8] = (Board[:ines_mapper_number] >> 8) & 0b1111
			if Board.key?(:nes2_0_submapper_number) == true
				header[8] |= 
					(Board[:nes2_0_submapper_number] & 0b1111) << 4
			end
		end
		f = File.open(destfilename, 'wb')
		f.write(header.pack('C*'))
		f.write(cpu_rom.pack('C*'))
		f.write(ppu_rom.pack('C*'))
		f.close
	end
	def driver_rom_dump
		if bus_open == false
			return false
		end
		if Board[:rom_dump_auto] == false
			rom_dump self, @ppuram
		else
			rom_register_init self
			@cpu.auto_rom_dump self
			if @ppuram == false
				@ppu.auto_rom_dump self
			end
		end
		bus_close
		true
	end
end
class Driver_dummy_dumper < Driver
	include Driver_dumper
	def initialize
		@cpu = Dummy_dumper_cpu.new
		@ppu = Dummy_dumper_ppu.new
	end
	def bus_open
		@vram_a10 = nil
		@ppuram = false
		true
	end
	def bus_close
	end
end
class Driver_bus_dumper < Driver
	include Driver_dumper
	include Bus_open
	def initialize
		@cpu = Bus_dumper_cpu.new
		@ppu = Bus_dumper_ppu.new
	end
	def bus_open
		if bus_open_ == false
			return false
		end
		@vram_a10 = nil
		if Board[:vram_a10_control] == false
			@vram_a10 = busreader_vram_connection == 5 ? 1 : 0
		end
		@ppuram = @ppu.memory_is_ram?
		gauge_range_set REGION_CPU, Board[:cpu_rom][:managed_rom_size]
		if @ppuram == false
			gauge_range_set REGION_PPU, Board[:ppu][:managed_rom_size]
		end
		true
	end
	def bus_close
		#gauge_close
		busreader_close
	end
end
