CC = clang
CFLAGS = -Wall -Werror -std=c99 -ferror-limit=2
CFLAGS += -O0 -g
OBJ = hamo.o reader_kazzo.o usb_device.o stdoutgauge.o
TARGET = hamo.exe
.PHONY = all clean
all: $(TARGET)
$(TARGET): $(OBJ)
	$(CC) -o $@ $^ -lmruby -lws2_32 -lz -lusb
