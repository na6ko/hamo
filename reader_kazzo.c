#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <libusb-compat/usb.h>
#include <unistd.h>
#include "kazzo_request.h"
#include "usb_device.h"
#include "reader_master.h"
//#include <kazzo_task.h>
enum{
	KAZZO_TASK_FLASH_IDLE = 0
};
//#include "reader_kazzo.h"

static inline usb_dev_handle *device_open(void)
{
	usb_dev_handle *handle = NULL;
	const unsigned char rawVid[2] = {USB_CFG_VENDOR_ID};
	const unsigned char rawPid[2] = {USB_CFG_DEVICE_ID};
	char vendor[] = {USB_CFG_VENDOR_NAME, 0};
	char product[] = {USB_CFG_DEVICE_NAME, 0};

	/* compute VID/PID from usbconfig.h so that there is a central source of information */
	int vid = (rawVid[1] << 8) | rawVid[0];
	int pid = (rawPid[1] << 8) | rawPid[0];

	if(usbOpenDevice(&handle, vid, vendor, pid, product, NULL, NULL, NULL) == 0){
		return handle;
	}
	return NULL;
}

static void dummy_value_add(void *g, enum region r, int length)
{
}
static const struct gauge dummy_gauge = {NULL, dummy_value_add};
struct reader_handle{
	usb_dev_handle *handle;
	void (*exception)(const char *str);
	const struct gauge *gauge;
};

static struct reader_handle *kazzo_open(void (*exception)(const char *str), struct gauge *gauge)
{
	static struct reader_handle H; //malloc/free は手間なので static 化. もし2台のデバイスを同時に操作する場合は malloc/free を利用すること.
	struct reader_handle *h = &H;
	usb_dev_handle *usb = device_open();
	if(usb == NULL){
		return NULL;
	}
	h->handle = usb;
	h->exception = exception;
	h->gauge = gauge;
	return h;
}
static void kazzo_close(struct reader_handle *h)
{
	usb_close(h->handle);
}

enum{
	TIMEOUT = 400
};
//-------- read sequence --------
static void device_read(struct reader_handle *h, enum request r, enum index index, long address, long length, uint8_t *data)
{
	int cnt = usb_control_msg(
		h->handle, 
		USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, 
		r, address, 
		index, (char *) data, length, TIMEOUT
	);
	if(cnt != length){
		h->exception(usb_strerror());
	}
}
#define GAUGE_SKIP (40)
static void read_main(struct reader_handle *h, const enum request r, enum index index, long address, long length, uint8_t *data, enum region region)
{
	const struct gauge *g = h->gauge;
	if(region == GAUGE_SKIP){
		g = &dummy_gauge;
	}
	const int l = READ_PACKET_SIZE;
	while(length >= l){
		device_read(
			h, r, index, address, l, data
		);
		data += l;
		address += l;
		length -= l;
		g->value_add(g->g, region, l);
	}
	if(length != 0){
		device_read(
			h, r, index, address, length, data
		);
		g->value_add(g->g, region, length);
	}
}
static void kazzo_memory_read(struct reader_handle *h, enum region region, long address, long length, uint8_t *data)
{
	assert(region == REGION_CPU || region == REGION_PPU);
	enum request r = REQUEST_CPU_READ;
	if(region == REGION_PPU){
		r = REQUEST_PPU_READ;
	}
	read_main(h, r, INDEX_IMPLIED, address, length, data, region);
}
//-------- write sequence --------
/*
When the host sends data have a lot of 0xff, v-usb losts some bits. 
To prevent losting bits, mask data xor 0xa5;
*/
static void device_write(struct reader_handle *h, enum request w, enum index index, long address, long length, const uint8_t *data)
{
	char d[FLASH_PACKET_SIZE];
	assert(length <= sizeof(d));
	int i;
	if(length == 0){
		length = 1;
		d[0] = 0;
	}else{
		memcpy(d, data, length);
		for(i = 0; i < length; i++){
			d[i] ^= 0xa5;
		}
	}
	int cnt = usb_control_msg(
		h->handle, 
		USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT,
		w, address, 
		index, d, length, TIMEOUT
	);
	if(cnt != length){
		h->exception(usb_strerror());
	}
}

static void kazzo_init(struct reader_handle *h)
{
	device_write(h, REQUEST_PHI2_INIT, INDEX_IMPLIED, 0, 0, NULL);
}

static void write_memory(struct reader_handle *h, enum request r, long address, long length, const uint8_t *data)
{
	while(length >= FLASH_PACKET_SIZE){
		long l = FLASH_PACKET_SIZE;
		device_write(h, r, INDEX_IMPLIED, address, l, data);
		address += l;
		data += l;
		length -= l;
	}
	if(length > 0){
		device_write(h, r, INDEX_IMPLIED, address, length, data);
	}
}
static void kazzo_cpu_register_write(struct reader_handle *h, long address, long length, const uint8_t *data, int increment, int mask)
{
	if(increment == 1 && mask == 0xffff){
		write_memory(h, REQUEST_CPU_WRITE_6502, address, length, data);
		return;
	}
	while(length > 0){
		write_memory(h, REQUEST_CPU_WRITE_6502, address, 1, data);
		data++;
		length -= 1;
		address += increment;
		address &= mask;
	}
}

static void kazzo_memory_write(struct reader_handle *h, enum region region, long address, long length, const uint8_t *data)
{
	assert(region == REGION_CPU || region == REGION_PPU);
	enum request r = REQUEST_CPU_WRITE_6502;
	if(region == REGION_PPU){
		r = REQUEST_PPU_WRITE;
	}
	write_memory(h, r, address, length, data);
}

static inline void pack_short_le(long l, uint8_t *t)
{
	t[0] = l & 0xff;
	t[1] = (l >> 8) & 0xff;
}

static enum index region_to_index(enum region r)
{
	assert(r == REGION_CPU || r == REGION_PPU);
	return r == REGION_CPU ? INDEX_CPU : INDEX_PPU;
}
static void kazzo_flash_config(struct reader_handle *h, enum region region, const long *command, long unit, bool retry)
{
	const int size = 2 * 4 + 1;
	uint8_t buf[size];
	uint8_t *t = buf;
	assert(unit >= 1 && unit < 0x400);
	pack_short_le(command[0], t);
	t += sizeof(uint16_t);
	pack_short_le(command[1], t);
	t += sizeof(uint16_t);
	pack_short_le(command[2], t);
	t += sizeof(uint16_t);
	pack_short_le(unit, t);
	t += sizeof(uint16_t);
	*t = retry == true ? 1 : 0;
	device_write(h, REQUEST_FLASH_CONFIG_SET, region_to_index(region), 0, size, buf);
}

static void kazzo_flash_erase(struct reader_handle *h, enum region region, enum erase area, long address)
{
	assert(area == ERASE_CHIP || area == ERASE_SECTOR);
	uint8_t d = area == ERASE_CHIP ? 0x10 : 0x30;
	device_write(h, REQUEST_FLASH_ERASE, region_to_index(region), address, 1, &d);
}
static void kazzo_flash_program_skip(struct reader_handle *h, enum region region, int length)
{
	assert(length == FLASH_PACKET_SIZE);
	h->gauge->value_add(h->gauge->g, region, length);
}
static void kazzo_flash_program_data(struct reader_handle *h, enum region region, long address, long length, const uint8_t *data)
{
	enum index index = region_to_index(region);
	assert(length == FLASH_PACKET_SIZE);

	device_write(h, REQUEST_FLASH_PROGRAM, index, address, FLASH_PACKET_SIZE, data);
	h->gauge->value_add(h->gauge->g, region, FLASH_PACKET_SIZE);
}
static void kazzo_flash_status(struct reader_handle *h, uint8_t s[2])
{
	read_main(h, REQUEST_FLASH_STATUS, INDEX_BOTH, 0, 2, s, GAUGE_SKIP);
}
static void kazzo_flash_device_get(struct reader_handle *h, enum region region, uint8_t s[2])
{
	read_main(h, REQUEST_FLASH_DEVICE, region_to_index(region), 0, 2, s, GAUGE_SKIP);
}
static uint8_t kazzo_vram_connection(struct reader_handle *h)
{
	uint8_t s;
	read_main(h, REQUEST_VRAM_CONNECTION, INDEX_IMPLIED, 0, 1, &s, GAUGE_SKIP);
	return s;
}
	
const struct reader_driver DRIVER_KAZZO = {
	.name = "kazzo",
	.open = kazzo_open, .close = kazzo_close,
	.init = kazzo_init,
	.vram_connection = kazzo_vram_connection,
	.cpu_register_write = kazzo_cpu_register_write,

	.memory_read = kazzo_memory_read, 
	.memory_write = kazzo_memory_write,

	.flash_status = kazzo_flash_status,
	.flash_config = kazzo_flash_config,
	.flash_erase = kazzo_flash_erase,
	.flash_program_data = kazzo_flash_program_data,
	.flash_program_skip = kazzo_flash_program_skip,
	.flash_device_get = kazzo_flash_device_get
};
