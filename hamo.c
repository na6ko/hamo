#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <zlib.h>
#include <mruby.h>
#include <mruby/array.h>
#include <mruby/string.h>
#include <mruby/compile.h>
#include <mruby/error.h>
#include "reader_master.h"
#include "stdoutgauge.h"

const struct reader_driver DRIVER_KAZZO;

static int script_load(mrb_state *m, const char *filename, mrb_value *rbfile)
{
	FILE *const f = fopen(filename, "r");
	if(f == NULL){
		printf("file %s is not found\n", filename);
		return -1;
	}
	int arena = mrb_gc_arena_save(m);
	{
		mrbc_context *c = mrbc_context_new(m);
		mrbc_filename(m, c, filename);
		*rbfile = mrb_load_file_cxt(m, f, c);
		mrbc_context_free(m, c);
	}
	fclose(f);
	if(m->exc != 0 && (mrb_nil_p(*rbfile) || mrb_undef_p(*rbfile))){
		mrb_print_error(m);
		//m->exc = 0;
		mrb_close(m);
		return -2;
	}
	mrb_gc_arena_restore(m, arena);
	return 0;
}

struct array{
	mrb_value mrb_array;
	mrb_int size;
	uint8_t *data;
};

static int array_into_uint8_t(mrb_state *m, struct array *t)
{
	t->size = ARY_LEN(RARRAY(t->mrb_array));
	if(t->size == 0){
		t->data = NULL;
		return 1;
	}
	t->data = malloc(t->size);
	for(mrb_int i = 0; i < t->size; i++){
		mrb_int v = mrb_fixnum(mrb_ary_ref(m, t->mrb_array, i));
		if(v < 0 || v >= 0x100){
			free(t->data);
			t->data = NULL;
			return 0;
		}
		t->data[i] = v;
	}
	return 1;
}
static mrb_value m_usleep(mrb_state *m, mrb_value self)
{
	mrb_value d;
	mrb_get_args(m, "i", &d);
	usleep(mrb_fixnum(d));
	return mrb_nil_value();
}
static mrb_value m__require(mrb_state *m, mrb_value self)
{
	mrb_value s, rbfile;
	mrb_get_args(m, "S", &s);
	script_load(m, mrb_str_to_cstr(m, s), &rbfile);
	return mrb_nil_value();
}
static mrb_value m_crc32_calc(mrb_state *m, mrb_value self)
{
	struct array t;
	mrb_get_args(m, "A", &t.mrb_array);
	if(array_into_uint8_t(m, &t) == 0){
		mrb_sys_fail(m, "uint8_t range error");
		return mrb_nil_value();
	}
	if(t.size == 0){
		return mrb_fixnum_value(0);
	}
	uint32_t crc = crc32(0L, Z_NULL, 0);
	crc = crc32(crc, t.data, t.size);
	free(t.data);
	return mrb_fixnum_value(crc);
}

static struct{
	const struct reader_driver *d;
	struct reader_handle *h;
	struct gauge *gauge;
}bus_driver;

static mrb_value m_gauge_range_set(mrb_state *m, mrb_value self)
{
	mrb_int region, range;
	mrb_get_args(m, "ii", &region, &range);
	assert(region == REGION_CPU || region == REGION_PPU);
	stdoutgauge_range_set(bus_driver.gauge, region, range);
	return mrb_nil_value();
}
static mrb_value m_gauge_close(mrb_state *m, mrb_value self)
{
	stdoutgauge_close(bus_driver.gauge);
	return mrb_nil_value();
}
static void driver_exception(const char *str)
{
	puts(str);
}
static mrb_value m_kazzo_open(mrb_state *m, mrb_value self)
{
	bus_driver.d = &DRIVER_KAZZO;
	bus_driver.gauge = stdoutgauge_init();
	bus_driver.h = bus_driver.d->open(driver_exception, bus_driver.gauge);
	if(bus_driver.h == NULL){
		return mrb_false_value();
	}
	bus_driver.d->init(bus_driver.h);
	return mrb_true_value();
}
static mrb_value m_kazzo_close(mrb_state *m, mrb_value self)
{
	bus_driver.d->close(bus_driver.h);
	m_gauge_close(m, self);
	bus_driver.h = NULL;
	return mrb_nil_value();
}
static mrb_value m_kazzo_memory_read(mrb_state *m, mrb_value self)
{
	assert(bus_driver.h != NULL);
	mrb_value m_data;
	mrb_int region, address, size;
	mrb_get_args(m, "iiiA", &region, &address, &size, &m_data);
	uint8_t *data = malloc(size);
	const uint8_t *d = data;
	bus_driver.d->memory_read(bus_driver.h, region, address, size, data);
	while(size != 0){
		mrb_ary_push(m, m_data, mrb_fixnum_value(*d));
		size -= 1;
		d++;
	}
	free(data);
	return mrb_nil_value();
}
static mrb_value m_kazzo_memory_write(mrb_state *m, mrb_value self)
{
	assert(bus_driver.h != NULL);
	struct array array;
	mrb_int address, region;
	mrb_get_args(m, "iiA", &region, &address, &array.mrb_array);
	array_into_uint8_t(m, &array);
	bus_driver.d->memory_write(bus_driver.h, region, address, array.size, array.data);
	free(array.data);
	return mrb_nil_value();
}
static mrb_value m_kazzo_cpu_register_write(mrb_state *m, mrb_value self)
{
	assert(bus_driver.h != NULL);
	struct array array;
	mrb_int address, increment, mask;
	mrb_get_args(m, "iAii", &address, &array.mrb_array, &increment, &mask);
	array_into_uint8_t(m, &array);
	if(array.size == 0){
		return mrb_nil_value();
	}
	bus_driver.d->cpu_register_write(bus_driver.h, address, array.size, array.data, increment, mask);
	free(array.data);
	return mrb_nil_value();
}
static mrb_value m_kazzo_vram_connection(mrb_state *m, mrb_value self)
{
	assert(bus_driver.h != NULL);
	return mrb_fixnum_value(bus_driver.d->vram_connection(bus_driver.h));
}
static mrb_value m_kazzo_flash_command_set(mrb_state *m, mrb_value self)
{
	mrb_int region, c0, c2, c5, program_unit;
	mrb_get_args(m, "iiiii", &region, &c0, &c2, &c5, &program_unit);
	const long c[3] = {c0, c2, c5};
	bus_driver.d->flash_config(bus_driver.h, region, c, program_unit, false);
	return mrb_nil_value();
}
static mrb_value m_kazzo_flash_id_get(mrb_state *m, mrb_value self)
{
	mrb_int region;
	mrb_get_args(m, "i", &region);
	uint8_t id[2];
	bus_driver.d->flash_device_get(bus_driver.h, region, id);
	mrb_value m_id = mrb_ary_new(m);
	for(int i = 0; i < sizeof(id); i++){
		mrb_ary_push(m, m_id, mrb_fixnum_value(id[i]));
	}
	return m_id;
}
static mrb_value m_kazzo_flash_erase(mrb_state *m, mrb_value self)
{
	mrb_int region, area, address;
	mrb_get_args(m, "iii", &region, &area, &address);
	bus_driver.gauge->str_set(bus_driver.gauge->g, region, "erase");
	bus_driver.d->flash_erase(bus_driver.h, region, area, address);
	return mrb_nil_value();
}
static mrb_value m_kazzo_flash_program_data(mrb_state *m, mrb_value self)
{
	mrb_int region, address;
	struct array array;
	mrb_get_args(m, "iiA", &region, &address, &array.mrb_array);
	array_into_uint8_t(m, &array);
	assert(array.size > 0);
	bus_driver.d->flash_program_data(bus_driver.h, region, address, array.size, array.data);
	free(array.data);
	return mrb_nil_value();
}
static mrb_value m_kazzo_flash_program_skip(mrb_state *m, mrb_value self)
{
	mrb_int region, length;
	mrb_get_args(m, "ii", &region, &length);
	bus_driver.d->flash_program_skip(bus_driver.h, region, length);
	return mrb_nil_value();
}
static mrb_value m_kazzo_flash_status(mrb_state *m, mrb_value self)
{
	mrb_int region;
	mrb_get_args(m, "i", &region);
	uint8_t s[2];
	bus_driver.d->flash_status(bus_driver.h, s);
	if(s[region] == 0){
		return mrb_true_value();
	}
	return mrb_false_value();
}
static const struct funclist{
	const char *name;
	mrb_value (*func)(mrb_state *m, mrb_value self);
	int argc;
}list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, ARGC}
#define K(NAME, ARGC) {"busreader_" #NAME, m_kazzo_##NAME, ARGC}
	M(usleep, 1), //i
	M(crc32_calc, 1), //a
	M(_require, 1), //s
	M(gauge_range_set, 2), //region, range
	M(gauge_close, 0),
	K(open, 0),
	K(close, 0),
	K(vram_connection, 0),
	K(memory_read, 4), //region, address, size, array
	K(memory_write, 3), //region, address, array
	K(cpu_register_write, 4), //address, data, increment, mask
	K(flash_command_set, 5), //region, 0000, 2aaa, 5555, unit
	K(flash_id_get, 1), //region
	K(flash_erase, 3), //region, area, address
	K(flash_program_data, 3), //region, address, data
	K(flash_program_skip, 2), //region, length
	K(flash_status, 1), ///region
	{NULL, NULL, 0}
#undef K
#undef M
};
static int script_load_multi(mrb_state *m, const char **b)
{
	mrb_value rbfile;
	int r = 0;
	while(*b != NULL){
		int r = script_load(m, *b, &rbfile);
		if(r != 0){
			return r;
		}
		b++;
	}
	return r;
}
static int script_exec(const char **filename, const char *funcname, int argc, char **c_argv)
{
#define SCRIPTARGNUM (20)
	if(argc > SCRIPTARGNUM){
		printf("argc is too many, argc > %d\n", SCRIPTARGNUM);
		return -1;
	}
	mrb_state *m = mrb_open();
	{
		const struct funclist *l = list;
		while(l->name != NULL){
			mrb_define_method(m, m->kernel_module, l->name, l->func, MRB_ARGS_REQ(l->argc));
			l++;
		}
	}
	static const char *basefile[] = {
		"base.rb", "driver.rb", "checker.rb", 
		"dumper.rb", "programmer.rb",
		NULL
	};
	int r = script_load_multi(m, basefile);
	if(r != 0){
		return r;
	}
	
	r = script_load_multi(m, filename);
	if(r != 0){
		return r;
	}
	mrb_value m_argv[SCRIPTARGNUM];
	for(int i = 0; i < argc; i++){
		m_argv[i] = mrb_str_new_cstr(m, c_argv[i]);
	}
	mrb_funcall_argv(m, mrb_top_self(m), mrb_intern_cstr(m, funcname), argc, m_argv);
	r = 0;
	if(m->exc != 0){
		mrb_print_error(m);
		//m->exc = 0;
		r = -2;
	}
	mrb_close(m);
	return r;
}
int main(int c, char **v)
{
	int r;
#define SCRIPTFILENUM (20)
	const char *scriptfilename[SCRIPTFILENUM + 1], **s = scriptfilename;
	do{
		r = getopt(c, v, "f:");
		switch(r){
		case 'f':
			if((s - scriptfilename) > SCRIPTFILENUM){
				puts("too many script filenames");
				return -1;
			}
			*s = optarg;
			s++;
			break;
		case -1:
			break;
		default:
			r = 1;
			c = -1;
			printf("unknown option '%c'\n", r);
			break;
		}
	}while(r != -1);
#undef SCRIPTFILENUM
	*s = NULL;
	if((c - optind) < 1){
		puts("hamo (options...) [functionname] (function arguments...)");
		puts("options:");
		puts("  -f filename  load script filename");
		return -1;
	}
	bus_driver.h = NULL;
	v += optind;
	c -= optind;

	const char *funcname = *v;
	v += 1;
	c -= 1;
	return script_exec(scriptfilename, funcname, c ,v);
}
