def screen_dump data
	i = 0
	str = ""
	data.each{|t|
		str << sprintf("%02x", t)
		case i
		when 3,7,11
			str << '-'
		when 15
			puts str
			str = ""
		else
			str << ' '
		end
		i += 1
		i &= 0xf
	}
end
def vram_test
	r = busreader_open
	puts r ? "ok" : "bad"
	if r == false
		return
	end
#H:9
#V:5
	p busreader_vram_connection
	busreader_close
end

def flash_test
	r = busreader_open
	if r == false
		return
	end
	busreader_cpu_register_write(0xa001, [0], 1, 0xffff)
	busreader_cpu_register_write(0x8000, [6, 0, 7, 1], 1, 0xffff)
	busreader_flash_command_set(REGION_CPU, 0x8000, 0x8000 | 0x2aaa, 0x8000 | 0x5555, 1)
	ar = busreader_flash_id_get(REGION_CPU)
	printf "%02x %02x\n", ar[0], ar[1]
	busreader_close
end

def dump_test
	r = busreader_open
	if r == false
		return
	end
	data = []
	busreader_cpu_register_write(0x8000, [6, 0, 7, 1], 1, 0xffff)
	
	busreader_memory_read(REGION_CPU, 0x8000, 0x10, data)
	busreader_memory_read(REGION_CPU, 0x80f0, 0x20, data)
	screen_dump data
	busreader_close
end

def crc32_listup data, pagesize
	offset = 0
	i = 0
	str = ""
	while offset < data.size
		if i == 0
			str = sprintf "%05x:", offset
		end
		str << sprintf("%04x", crc32_calc(data[offset, pagesize]) & 0xffff)
		case i
		when 3
			str << '-'
		when 7
			puts str
			str = ""
		else
			str << " "
		end
		i += 1
		i &= 0b111
		offset += pagesize
	end
	if str != ""
		puts str
	end
end

NESHEADER_0_3 = [0x4e, 0x45, 0x53, 0x1a]
def rompage_crc(srcfilename, region, pagesize = nil)
	f = File.open(srcfilename, 'rb')
	header = f.read(0x10).unpack('C*')
	if header[0,4] != NESHEADER_0_3
		puts "NES header error"
		return
	end
	rom = nil
	case region
	when "CPU"
		if pagesize == nil
			pagesize = 0x4000
		else
			pagesize = pagesize.to_i(0x10)
		end
		rom = f.read(header[4] * 0x4000).unpack('C*')
	when "PPU"
		if pagesize == nil
			pagesize = 0x2000
		else
			pagesize = pagesize.to_i(0x10)
		end
		f.seek(0x10+header[4] * 0x4000)
		rom = f.read(header[5] * 0x2000).unpack('C*')
	end
	f.close
	crc32_listup rom, pagesize
end
def pad(pd, src)
	pd = pd.split(":")
	bottom = 0x4000
	if pd.size >= 3
		bottom = pd[2].to_i(0)
	end
	dest = Array.new(pd[1].to_i(0), 0xff)
	case pd[0]
	when 't'
		dest[0, src.size - bottom] = src[0, src.size - bottom]
		dest[dest.size - bottom, bottom] = src[src.size - bottom, bottom]
	when 'b'
		dest[dest.size - src.size, src.size] = src
	end
	dest
end
def padding(srcfilename, destfilename, cpu_padding, ppu_padding = nil)
	f = File.open(srcfilename, 'rb')
	header = f.read(0x10).unpack('C*')
	if header[0,4] != NESHEADER_0_3
		puts "NES header error"
		return
	end
	prgrom = pad(cpu_padding, f.read(header[4] * 0x4000).unpack('C*'))
	chrrom = []
	if header[5] != 0
		chrrom = pad(ppu_padding, f.read(header[5] * 0x2000).unpack('C*'))
	end
	f.close
	header[4] = prgrom.size / 0x4000
	header[5] = chrrom.size / 0x2000
	f = File.open(destfilename, 'wb')
	f.write(header.pack('C*'))
	f.write(prgrom.pack('C*'))
	f.write(chrrom.pack('C*'))
	f.close
end
def hoge
	offset = 0
	h = {}
	while offset < 0x11000
		t = SectorRange.new(offset, offset + 0x1000 - 1)
		h[t] = false
		offset += 0x1000
	end

	offset = 0
	j = {}
	while offset < 0x80000
		t = SectorRange.new(offset, offset + 0x10000 - 1)
		j[t] = false
		offset += 0x10000
	end
	p h,j
	p h[0],j[0]
end
