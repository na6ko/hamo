#ifndef _READER_MASTER_
#include <stdbool.h>
#include <stdint.h>
enum region{REGION_CPU = 0, REGION_PPU};
enum erase{ERASE_CHIP = 0, ERASE_SECTOR};
struct gauge{
	void *g;
	void (*value_add)(void *g, enum region r, int length);
	void (*str_set)(void *g, enum region r, const char *str);
};
struct reader_handle;
struct reader_driver{
	const char *name;
	struct reader_handle *(*open)(void (*exception)(const char *str), struct gauge *g);
	void (*close)(struct reader_handle *h);
	void (*init)(struct reader_handle *h);
	uint8_t (*vram_connection)(struct reader_handle *h);
	void (*cpu_register_write)(struct reader_handle *h, long address, long length, const uint8_t *data, int increment, int mask);
	void (*memory_read)(struct reader_handle *h, enum region region, long address, long length, uint8_t *data);
	void (*memory_write)(struct reader_handle *h, enum region region, long address, long length, const uint8_t *data);
	void (*flash_config)(struct reader_handle *h, enum region region, const long *command, long unit, bool retry);
	void (*flash_erase)(struct reader_handle *h, enum region region, enum erase area, long address);
	void (*flash_program_data)(struct reader_handle *h, enum region region, long address, long length, const uint8_t *data);
	void (*flash_program_skip)(struct reader_handle *h, enum region region, int length);
	void (*flash_status)(struct reader_handle *h, uint8_t s[2]);
	void (*flash_device_get)(struct reader_handle *h, enum region region, uint8_t s[2]);
};
#endif
