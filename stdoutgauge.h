#ifndef _STDOUTGAUGE_
#define _STDOUTGAUGE_
struct gauge *stdoutgauge_init(void);
void stdoutgauge_range_set(struct gauge *t, enum region r, int range);
void stdoutgauge_close(struct gauge *t);
#endif
