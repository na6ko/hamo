#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "reader_master.h"

struct gaugevalue{
	const char *name;
	int offset, max;
	const char *str;
};
static void render(struct gaugevalue * v)
{
	char str[2][0x10];
	for(int i = 0; i < 2; i++, v++){
		if(v->max == 0){
			str[i][0] = '\0';
			continue;
		}
		if(v->str != NULL){
			snprintf(str[i], sizeof(str[0]), "%s:%s", v->name, v->str);
		}else{
			snprintf(str[i], sizeof(str[0]), "%s:%4d%%", v->name, (v->offset * 100) / v->max);
		}
	}
	printf("\r%s %s", str[0], str[1]);
	fflush(stdout);
}
static void value_add(void *g, enum region r, int length)
{
	struct gaugevalue *v = g;
	v += r;
	if(v->name == NULL){
		return;
	}
	v->offset += length;
	v->str = NULL;
	render(g);
}
static void str_set(void *g, enum region r, const char *str)
{
	struct gaugevalue *v = g;
	v += r;
	if(v->name == NULL){
		return;
	}
	v->str = str;
	render(g);
}
struct gauge *stdoutgauge_init(void)
{
	static struct gauge gauge;
	struct gauge *g = &gauge;
	static struct gaugevalue value[2];
	memset(value, 0, sizeof(value));
	g->g = value;
	g->value_add = value_add;
	g->str_set = str_set;
	return g;
}
void stdoutgauge_range_set(struct gauge *t, enum region r, int range)
{
	struct gaugevalue *v = t->g;
	v += r;
	switch(r){
	case REGION_CPU:
		v->name = "CPU";
		break;
	case REGION_PPU:
		v->name = "PPU";
		break;
	}
	v->offset = 0;
	v->max = range;
}
void stdoutgauge_close(struct gauge *t)
{
	struct gaugevalue *v = t->g;
	memset(v, 0, sizeof(struct gaugevalue) * 2);
	char str[4+4+1+4+4+1];
	memset(str, ' ', sizeof(str));
	str[sizeof(str) - 1] = '\0';
	printf("%s\r", str);
	fflush(stdout);
}
