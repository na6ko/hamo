Board = {
	:ines_mapper_number => 1,
	:vram_a10_control => true,
	:rom_dump_auto => true,
	:cpu_rom => {
		:flash_program_auto => true,
		:managed_rom_size => 0x4000 * 0x10,
		:rom_bank => [
			{:address => 0x8000, :size => 0x4000},
			{:address => 0xc000, :size => 0x4000}
		]
	}, :ppu => {
		:flash_program_auto => true,
		:managed_rom_size => 0x1000 * 0x20,
		:rom_bank => [
			{:address => 0x0000, :size => 0x1000},
			{:address => 0x1000, :size => 0x1000}
		]
	}
}
def rom_register_init(d)
	v = 0b1_11_00
	ar = [0x80]
	5.times{
		ar << v
		v >>= 1
	}
	d.cpu.register_write(0x8000, ar)
end
def mmc1_write(d, address, data)
	ar = []
	5.times{
		ar << data
		data >>= 1
	}
	d.cpu.register_write(address, ar)
end
def rom_bank_control_cpu(d, memory_offset, cpu_address, size)
	page = memory_offset / 0x4000

	case cpu_address & 0xc000
	when 0x8000
	when 0xc000
		return
	end

	page |= 1 << 4
	mmc1_write(d, 0xe000, page)
end
def rom_bank_control_ppu(d, memory_offset, ppu_address, size)
	page = memory_offset / 0x1000
	a = nil
	
	case ppu_address & 0x1000
	when 0x0000
		a = 0xa000
	when 0x1000
		a = 0xc000
	end
	
	while size != 0
		mmc1_write(d, a, page)
		size -= 0x1000
		a += 0x2000
		page += 1
	end
end
