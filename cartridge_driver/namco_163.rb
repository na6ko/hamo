Board = {
	:ines_mapper_number => 19,
	:vram_a10_control => true,
	:rom_dump_auto => true,
	:cpu_rom => {
		:flash_program_auto => true,
		:managed_rom_size => 0x2000 * 0x20,
		:rom_bank => [
			{:address => 0x8000, :size => 0x2000},
			{:address => 0xa000, :size => 0x2000},
			{:address => 0xc000, :size => 0x4000}
		]
	}, :ppu => {
		:flash_program_auto => true,
		:managed_rom_size => 0x400 * 0x100,
		:rom_bank => [
			{:address => 0x0000, :size => 0x0400},
			{:address => 0x0400, :size => 0x0800},
			{:address => 0x1000, :size => 0x1000},
			{:address => 0x2000, :size => 0x1000}
		]
	}
}
def rom_register_init(d)
	d.cpu.register_write(0xf800, 0)
end
def rom_bank_control_cpu(d, memory_offset, cpu_address, size)
	a = nil
	case cpu_address & 0xe000
	when 0x8000
		a = 0xe000
	when 0xa000
		a = 0xe800
	when 0xc000
		a = 0xf000
	when 0xe000
		return
	end
	data = []
	page = memory_offset / 0x2000
	while size > 0
		if cpu_address == 0xa000
			data << (page | 0xc0)
		else
			data << (page & 0xbf)
		end
		page += 1
		cpu_address += 0x2000
		if cpu_address >= 0xe000
			break
		end
		size -= 0x2000
	end
	d.cpu.register_write(a, data, 0x800)
end
def rom_bank_control_ppu(d, memory_offset, ppu_address, size)
	a = nil
	case ppu_address & 0x3c00
	when 0x0000
		a = 0x8000
	when 0x0400
		a = 0x8800
	when 0x1000
		a = 0xa000
	when 0x2000
		a = 0xc000
	end
	data = []
	page = memory_offset / 0x400
	while size > 0
		data << page
		page += 1
		size -= 0x400
	end
	if memory_offset >= 0x38000
		d.cpu.register_write(0xe800, 0xc0)
	end
	d.cpu.register_write(a, data, 0x800)
end
