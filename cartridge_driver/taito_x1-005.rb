Board = {
	:ines_mapper_number => 80,
	:vram_a10_control => true,
	:rom_dump_auto => true,
	:cpu_rom => {
		:flash_program_auto => true,
		:managed_rom_size => 0x2000 * 0x40,
		:rom_bank => [
			{:address => 0x8000, :size => 0x2000},
			{:address => 0xa000, :size => 0x2000},
			{:address => 0xc000, :size => 0x4000}
		]
	}, :ppu => {
		:flash_program_auto => true,
		:managed_rom_size => 0x400 * 0x100,
		:rom_bank => [
			{:address => 0x0000, :size => 0x0800},
			{:address => 0x0800, :size => 0x0800},
			{:address => 0x1000, :size => 0x1000},
		]
	}
}
def rom_register_init(d)
	d.cpu.register_write(0x7ef6, [0,0,0,0])
end
def rom_bank_control_cpu(d, memory_offset, cpu_address, size)
	case cpu_address & 0xe000
	when 0x8000
		a = 0x7efa
	when 0xa000
		a = 0x7efc
	when 0xc000
		a = 0x7efe
	when 0xe000
		return
	end
	page = memory_offset / 0x2000
	data = []
	while size > 0
		data << page
		page += 1
		cpu_address += 0x2000
		if cpu_address >= 0xe000
			break
		end
		size -= 0x2000
	end
	d.cpu.register_write(a, data, 2)
end
def rom_bank_control_ppu(d, memory_offset, ppu_address, size)
	case ppu_address & 0x3c00
	when 0x0000
		a = 0x7ef0
	when 0x0800
		a = 0x7ef1
	when 0x1000
		a = 0x7ef2
	end
	data = []
	page = memory_offset / 0x400
	while size > 0
		data << page

		add = 1
		if ppu_address < 0x1000
			add = 2
		end
		page += add
		ppu_address += add * 0x400
		size -= add * 0x400
	end
	d.cpu.register_write(a, data)
end
