Board = {
	:ines_mapper_number => 2,
	:vram_a10_control => false,
	:rom_dump_auto => true,
	:cpu_rom => {
		:flash_program_auto => true,
		:managed_rom_size => 0x20000,
		:rom_bank => [
			{:address => 0x8000, :size => 0x4000},
			{:address => 0xc000, :size => 0x4000}
		]
	}, :ppu => {
		:flash_program_auto => true,
		:managed_rom_size => 0
	}
}
def rom_register_init(d)
end
def rom_bank_control_cpu(d, memory_offset, cpu_address, size)
	if cpu_address == 0xc000
		return
	end
	d.cpu.register_write(0x8000, memory_offset / 0x4000)
end
