#VRC2.REGISTER[1:0] = CPU.A[1:0]
Board = {
	:ines_mapper_number => 0x17,
	:vram_a10_control => true,
	:rom_dump_auto => true,
	:cpu_rom => {
		:flash_program_auto => true,
		:managed_rom_size => 0x2000 * 0x20,
		:rom_bank => [
			{:address => 0x8000, :size => 0x2000},
			{:address => 0xa000, :size => 0x2000},
			{:address => 0xc000, :size => 0x4000}
		]
	}, :ppu => {
		:flash_program_auto => true,
		:managed_rom_size => 0x400 * 0x100,
		:rom_bank => [
			{:address => 0x0000, :size => 0x0400},
			{:address => 0x0400, :size => 0x0400},
			{:address => 0x1000, :size => 0x1000}
		]
	}
}
def rom_register_init(d)
end
def rom_bank_control_cpu(d, memory_offset, cpu_address, size)
	a = nil
	case cpu_address & 0xf000
	when 0x8000
		a = 0x8000
	when 0xa000
		a = 0xa000
	when 0xc000
		return
	end

	page = memory_offset / 0x2000
	while size > 0
		d.cpu.register_write(a, page)
		a += 0x2000
		page += 1
		size -= 0x2000
	end
end
def rom_bank_control_ppu(d, memory_offset, ppu_address, size)
	a = nil
	case ppu_address & 0x3c00
	when 0x0000
		a = 0xb000
	when 0x0400
		a = 0xb002
	when 0x1000
		a = 0xd000
	end
	page = memory_offset / 0x0400
	while size > 0
		d.cpu.register_write(a, [page, page >> 4])
		if (a & 2) == 2
			a &= 0xf000
			a += 0x1000
		else
			a += 2
		end
		page += 1
		size -= 0x400
	end
end
