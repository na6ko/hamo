Board = {
	:ines_mapper_number => 4,
	:vram_a10_control => true,
	:rom_dump_auto => false,
	:cpu_rom => {
		:flash_program_auto => true,
		:managed_rom_size => 0x2000 * 0x40,
		:rom_bank => [
			{:address => 0x8000, :size => 0x2000},
			{:address => 0xa000, :size => 0x2000},
			{:address => 0xc000, :size => 0x4000}
		]
	}, :ppu => {
		:flash_program_auto => true,
		:managed_rom_size => 0x400 * 0x100,
		:rom_bank => [
			{:address => 0x0000, :size => 0x0800},
			{:address => 0x0800, :size => 0x0800},
			{:address => 0x1000, :size => 0x1000}
		]
	}
}
def rom_register_init(d)
	d.cpu.register_write(0xa001, 0); #disable W-RAM
end
def rom_bank_control_cpu(d, memory_offset, cpu_address, size)
	page = memory_offset / 0x2000
	reg = nil

	case cpu_address & 0xf000
	when 0x8000
		reg = 6
	when 0xa000
		reg = 7
	when 0xc000
		return
	end

	ar = []
	while size != 0
		ar << reg
		ar << page
		reg += 1
		page += 1
		size -= 0x2000
	end
	d.cpu.register_write(0x8000, ar)
end
def rom_bank_control_ppu(d, memory_offset, ppu_address, size)
	page = memory_offset / 0x400
	reg = nil
	
	case ppu_address & 0x3c00
	when 0x0000
		reg = 0
	when 0x0800
		reg = 1
	when 0x1000
		reg = 2
	end
	
	ar = []
	while size != 0
		ar << reg
		ar << page
		reg += 1
		if ppu_address < 0x1000
			page += 2
			ppu_address += 0x800
			size -= 0x800
		else
			page += 1
			ppu_address += 0x400
			size -= 0x400
		end
	end
	d.cpu.register_write(0x8000, ar)
end
def rom_dump(c, ppu_ram)
	#CPU
	rom_offset = 0
	while rom_offset < (0x80000 - 0x04000)
		rom_bank_control_cpu(c, rom_offset, 0x8000, 0x4000)
		c.cpu.memory_read(0x8000, 0x4000)
		rom_offset += 0x4000
	end
	c.cpu.memory_read(0xc000, 0x4000)
	if ppu_ram == true
		return
	end
	#PPU
	rom_offset = 0
	while rom_offset < 0x40000
		rom_bank_control_ppu(c, rom_offset, 0x0000, 0x2000)
		c.ppu.memory_read(0x0000, 0x2000)
		rom_offset += 0x2000
	end
end
=begin
CPU memory bank for T*ROM
cpu address|rom address    |page|task
$8000-$9fff|0x02000-0x03fff|1   |write 0x2aaa
$a000-$bfff|n * 0x2000     |n   |write area
$c000-$ffff|0x7c000-0x7ffff|fix |write 0x5555, boot area

PPU memory bank for TLROM TKROM TKSROM
ppu address|rom address    |page|task
$0000-$07ff|0x02800-0x02fff|0x0a|write 0x2aaa
$0800-$0fff|0x05000-0x057ff|0x14|write 0x5555
$1000-$1fff|n * 0x1000     |n   |write area
=end

def flash_program_cpu(d, dest_offset, dest_size)
	d.cpu.flash_command_set(0x0000, 0xa000, 0x2000)
	d.cpu.flash_command_set(0x2aaa, 0x8000, 0x2000)
	d.cpu.flash_command_set(0x5555, 0xc000, 0x4000)
	d.cpu.register_write(0x8000, [7, 0, 6, 1])
	d.cpu.flash_programming_start
	while dest_size > 0x04000
		rom_bank_control_cpu(d, dest_offset, 0xa000, 0x2000)
		d.cpu.flash_program(0xa000, 0x2000)
		dest_offset += 0x2000
		dest_size -= 0x2000
	end
	d.cpu.flash_program(0xC000, 0x4000)
end
def flash_program_ppu(d, dest_offset, dest_size)
	d.ppu.flash_command_set(0x0000, 0x1000, 0x1000)
	d.ppu.flash_command_set(0x2aaa, 0x0000, 0x0800)
	d.ppu.flash_command_set(0x5555, 0x0800, 0x0800)
	d.cpu.register_write(0x8000, [2, 0, 0, 0xa, 1, 0x14])
	d.ppu.flash_programming_start
	while dest_size > 0
		rom_bank_control_ppu(d, dest_offset, 0x1000, 0x1000)
		d.ppu.flash_program(0x1000, 0x1000)
		dest_offset += 0x1000
		dest_size -= 0x1000
	end
end

