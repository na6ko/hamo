Board = {
	:ines_mapper_number => 3,
	:vram_a10_control => false,
	:rom_dump_auto => true,
	:cpu_rom => {
		:flash_program_auto => true,
		:managed_rom_size => 0x8000,
		:rom_bank => [
			{:address => 0x8000, :size => 0x8000}
		]
	}, :ppu => {
		:flash_program_auto => true,
		:managed_rom_size => 0x8000,
		:rom_bank => [
			{:address => 0x0000, :size => 0x2000},
		]
	}
}
def rom_register_init(d)
end
def rom_bank_control_cpu(d, memory_offset, cpu_address, size)
end
def rom_bank_control_ppu(d, memory_offset, ppu_address, size)
	#no support diode protection by data bit7:4
	d.cpu.register_write(0xc000, memory_offset / 0x2000)
end
